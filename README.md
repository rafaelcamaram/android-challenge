# Android Test

This is an app that allows users to make searches for locations and present the results in a list, allowing them to be presented in a map view inside the app.

## Preview

![Scheme](./app1.jpg)
![Scheme](./app2.jpg)

## Status - Acceptance Criteria:

- [x] The app should support Android 4 and later. The look and feel is the system's default.
- [x] Text field on the top of the screen.
- [x] Tapping search button makes the search through the call to the Google Maps API.
- [x] The UI must be responsive while searching through the API call.
- [x] The results must be listed in the same order as received by the API response. Each item in the list should show the name of the related location.
- [x] If there is only one result, display only one row in the list.
- [x] **(+/-)** If there are more than one results, show a row for `Display All on Map` in a separated section.
- [x] Selecting this row would display the map with all results centered.
- [x] If there is no results, a `No Results` text should be displayed in the view instead of the result rows.
- [x] Selecting one item in the list should present a map view with all result items presented as markers. The selected marker should be centered. Feel free to set the zoom level.
- [x] Selecting a marker in the map view should present its location name and coordinates in the marker title.
- [ ] In the Map screen, add a `Save` button to the action bar, allowing the user to save the selected result object. This action should save the current object to a SQLite database and the object should be unique in the database.
    * This is not applicable to the `Display All on Map` option.
- [ ] Case the object has been previously saved, the "Save" button should become into a `Delete` button. Tapping this button will delete the current object from the database, after popping up a dialog confirming the deletion. User should be able to `Cancel` or `Delete` the item from the presented dialog.
    * This is not applicable to the `Display All on Map` option.
- [ ] Provide unit tests for models, activities and fragments, using the Android Testing API.

## Comments

- I split the project structure into a model and a service layer. The service layer is an interface that is responsible for setting all the endpoints and model is the responsible for representing data from Google Maps API.
- I'm using the `retrofit2` in order to transform our API interfaces into callable objects. And the `converter-gson` in order to be the JSON parser.
- If I had more time, I would improve the `Display all on map` button in order to match to the Acceptance Criteria that were given and also I would implement the **SQLite layer**.

## Colaborators
- [Rafael Camara Magalhaes](https://github.com/rafaelcamaram)

## Contribution

Made with :heart: by coffee
