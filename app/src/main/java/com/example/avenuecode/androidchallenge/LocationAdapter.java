package com.example.avenuecode.androidchallenge;

/**
 * Created by avenuecode on 18/12/2017.
 */

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.avenuecode.androidchallenge.model.Location;
import com.example.avenuecode.androidchallenge.model.Result;
import com.example.avenuecode.androidchallenge.utils.Color;
import com.example.avenuecode.androidchallenge.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.ViewHolder> {

    private List<Result> countryList;
    private LayoutInflater inflater;

    public class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout container;
        TextView formattedAddress;
        TextView geoLocation;
        public View layout;

        public ViewHolder(View v) {
            super(v);
            layout = v;
            formattedAddress = v.findViewById(R.id.formatted_address);
            geoLocation = v.findViewById(R.id.geo_location);
            container = v.findViewById(R.id.item_list_container);
        }
    }

    public LocationAdapter(Context applicationContext) {
        this.countryList = new ArrayList<>();
        inflater = (LayoutInflater.from(applicationContext));
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());
        View v = inflater.inflate(R.layout.list_item, parent, false);
        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final Result item = countryList.get(position);
        final Location location = item.getGeometry().getLocation();
        final String formattedAddress = item.getFormattedAddress();
        final String locationStr = location.getLat() + ", " + location.getLng();

        getItemBackgroundColor(position, holder.itemView, holder.container);

        holder.formattedAddress.setText(formattedAddress);
        holder.geoLocation.setText(locationStr);

        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mapActivity = new Intent(v.getContext(), MapsActivity.class);
                Bundle bundle = new Bundle();

                ArrayList<String> formattedCountriesAddresses = new ArrayList<>();
                ArrayList<String> latLngLocations = new ArrayList<>();

                for (Result result : countryList) {
                    formattedCountriesAddresses.add(result.getFormattedAddress());
                    latLngLocations.add(result.getGeometry().getLocation().getLat() + "," + result.getGeometry().getLocation().getLng());
                }

                bundle.putStringArrayList(Constants.FORMATTED_COUNTRIES_ADDRESSES, formattedCountriesAddresses);
                bundle.putStringArrayList(Constants.LAT_LNG_LOCATIONS, latLngLocations);

                bundle.putDouble(Constants.CURRENT_LATITUDE, location.getLat());
                bundle.putDouble(Constants.CURRENT_LONGITUDE, location.getLng());
                bundle.putString(Constants.CURRENT_FORMATTED_ADDRESS, item.getFormattedAddress());
                bundle.putBoolean(Constants.IS_BOUND, false);
                mapActivity.putExtras(bundle);

                v.getContext().startActivity(mapActivity);
            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return countryList.size();
    }

    public void setCountryList(List<Result> countryList) {
        this.countryList = countryList;
    }

    public List<Result> getCountryList() {
        return this.countryList;
    }

    public void clearCountryList() {
        this.countryList.clear();
    }

    public void addAllCountryList(List<Result> list) {
        this.countryList.addAll(list);
    }

    public void add(int position, Result item) {
        countryList.add(position, item);
        notifyItemInserted(position);
    }

    public void remove(int position) {
        countryList.remove(position);
        notifyItemRemoved(position);
    }

    private void getItemBackgroundColor(int i, View view, LinearLayout itemListContainer) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int hashColor;
            switch (i % 3) {
                case 0:
                    hashColor = Color.WHITE;
                    break;
                case 1:
                    hashColor = Color.YELLOW;
                    break;
                default:
                    hashColor = Color.GREEN;
                    break;
            }

            itemListContainer.setBackgroundTintList(view.getResources().getColorStateList(hashColor));
        }
    }
}