package com.example.avenuecode.androidchallenge;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.avenuecode.androidchallenge.model.Response;
import com.example.avenuecode.androidchallenge.model.Result;
import com.example.avenuecode.androidchallenge.service.GoogleService;
import com.example.avenuecode.androidchallenge.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private EditText txtLocation;
    private TextView lblError;
    private Button btnFetch, btnDisplayAll;
    private RecyclerView simpleList;

    private LocationAdapter locationAdapter;
    public static final String BASE_URL = "https://maps.googleapis.com/maps/api/";
    private GoogleService googleService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Gson gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .create();

        // Initializing Retrofit
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        googleService = retrofit.create(GoogleService.class);

        // ListView
        simpleList = findViewById(R.id.simpleListView);

        simpleList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        locationAdapter = new LocationAdapter(getApplicationContext());
        simpleList.setAdapter(locationAdapter);
        simpleList.addOnScrollListener(onScrollListenerHandler());
        // UI Elements
        lblError = findViewById(R.id.lblError);
        lblError.setVisibility(View.GONE);

        txtLocation = findViewById(R.id.txtLocation);
        txtLocation.setOnEditorActionListener(txtLocationOnEditorActionHandler());

        btnFetch = findViewById(R.id.btnFetch);
        btnFetch.setOnClickListener(btnFetchOnClickHandler());

        btnDisplayAll = findViewById(R.id.btnDisplayAll);
        btnDisplayAll.setVisibility(View.GONE);
        btnDisplayAll.setOnClickListener(btnDisplayAllOnClickHandler());
    }

    @NonNull
    private RecyclerView.OnScrollListener onScrollListenerHandler() {
        return new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (dy > 0 && btnDisplayAll.getVisibility() == View.VISIBLE) {
                    ObjectAnimator fadeOut = ObjectAnimator.ofFloat(btnDisplayAll, "alpha", 1f, 0f);
                    fadeOut.setDuration(500);

                    final AnimatorSet mAnimationSet = new AnimatorSet();

                    mAnimationSet.play(fadeOut);

                    mAnimationSet.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            btnDisplayAll.setVisibility(View.GONE);
                        }
                    });
                    mAnimationSet.start();
                }
                if (dy < 0 && btnDisplayAll.getVisibility() == View.GONE) {
                    btnDisplayAll.setVisibility(View.VISIBLE);
                    ObjectAnimator fadeIn = ObjectAnimator.ofFloat(btnDisplayAll, "alpha", 0f, 1f);
                    fadeIn.setDuration(500);

                    final AnimatorSet mAnimationSet = new AnimatorSet();

                    mAnimationSet.play(fadeIn);

                    mAnimationSet.addListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                        }
                    });
                    mAnimationSet.start();

                }
            }
        };
    }

    @NonNull
    private View.OnClickListener btnFetchOnClickHandler() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetchLocationAddressData();
            }
        };
    }

    @NonNull
    private View.OnClickListener btnDisplayAllOnClickHandler() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mapActivity = new Intent(getApplicationContext(), MapsActivity.class);
                Bundle bundle = new Bundle();

                ArrayList<String> formattedCountriesAddresses = new ArrayList<>();
                ArrayList<String> latLngLocations = new ArrayList<>();

                for (Result result : locationAdapter.getCountryList()) {
                    formattedCountriesAddresses.add(result.getFormattedAddress());
                    latLngLocations.add(result.getGeometry().getLocation().getLat() + "," + result.getGeometry().getLocation().getLng());
                }

                bundle.putStringArrayList(Constants.FORMATTED_COUNTRIES_ADDRESSES, formattedCountriesAddresses);
                bundle.putStringArrayList(Constants.LAT_LNG_LOCATIONS, latLngLocations);
                bundle.putBoolean(Constants.IS_BOUND, true);

                mapActivity.putExtras(bundle);

                getApplicationContext().startActivity(mapActivity);
            }
        };
    }

    @NonNull
    private TextView.OnEditorActionListener txtLocationOnEditorActionHandler() {
        return new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    fetchLocationAddressData();
                    return true;
                }
                return false;
            }
        };
    }

    private void fetchLocationAddressData() {
        clearList();
        setListError(R.string.loading);

        String txtLocationValue = this.txtLocation.getText().toString();
        if (txtLocationValue.length() > 0) {
            Call<Response> call = this.googleService.listAddress(txtLocationValue);

            call.enqueue(new Callback<Response>() {
                @Override
                public void onResponse(@NonNull Call<Response> call, @NonNull retrofit2.Response<Response> response) {

                    try {
                        List<Result> resultList = response.body().getResults();

                        if (resultList != null && resultList.size() > 0) {
                            hideListError();
                            updateListContent(resultList);
                        } else {
                            setListError(R.string.locationNotFound);
                        }
                    } catch (NullPointerException e) {
                        setListError(R.string.onFailureString);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<Response> call, @NonNull Throwable t) {
                    setListError(R.string.onFailureString);
                }
            });
        } else {
            setListError(R.string.withoutLocationAddress);
        }
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        assert imm != null;
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    private void updateListContent(@NonNull List<Result> responseList) {
        locationAdapter.clearCountryList();
        locationAdapter.addAllCountryList(responseList);
        locationAdapter.notifyDataSetChanged();

        if (responseList.size() == 1) {
            btnDisplayAll.setVisibility(View.GONE);
        } else if (responseList.size() > 1) {
            btnDisplayAll.setVisibility(View.VISIBLE);
        }
    }

    private void clearList() {
        btnDisplayAll.setVisibility(View.GONE);
        locationAdapter.clearCountryList();
        locationAdapter.notifyDataSetChanged();
    }

    private void setListError(int errorString) {
        lblError.setText(errorString);
        lblError.setVisibility(View.VISIBLE);
    }

    private void hideListError() {
        lblError.setVisibility(View.GONE);
        txtLocation.setText(null);
    }
}
