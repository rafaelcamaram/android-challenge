package com.example.avenuecode.androidchallenge;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.avenuecode.androidchallenge.utils.Constants;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        // Receiving data from MainActivity
        Bundle bundle = getIntent().getExtras();

        assert bundle != null;
        ArrayList<String> formattedCountriesAddresses = bundle.getStringArrayList(Constants.FORMATTED_COUNTRIES_ADDRESSES);
        ArrayList<String> latLngLocations = bundle.getStringArrayList(Constants.LAT_LNG_LOCATIONS);

        Double currentLatitude = bundle.getDouble(Constants.CURRENT_LATITUDE);
        Double currentLongitude = bundle.getDouble(Constants.CURRENT_LONGITUDE);
        String formatted_address = bundle.getString(Constants.CURRENT_FORMATTED_ADDRESS);
        boolean isBound = bundle.getBoolean(Constants.IS_BOUND);
        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        // Adding unselected locations
        assert formattedCountriesAddresses != null;
        for (int i = 0; i < formattedCountriesAddresses.size(); i++) {
            String title = formattedCountriesAddresses.get(i);
            assert latLngLocations != null;
            String[] latLng = latLngLocations.get(i).split(",");

            LatLng coord = new LatLng(Double.parseDouble(latLng[0]), Double.parseDouble(latLng[1]));
            builder.include(coord);

            LatLng markerLatLng = new LatLng(Double.parseDouble(latLng[0]), Double.parseDouble(latLng[1]));
            googleMap.addMarker(new MarkerOptions().position(markerLatLng).title(title + " | " + latLng[0] + ", " + latLng[1]));
        }
        LatLngBounds bounds = builder.build();

        // Choosing the map position
        if (isBound) {
            // It's executed when we just clicked on Display All button -> Center
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, 10);
            googleMap.moveCamera(cameraUpdate);
        } else {
            // Adding selected location
            LatLng currentMarketLatLng = new LatLng(currentLatitude, currentLongitude);
            googleMap.addMarker(new MarkerOptions().position(currentMarketLatLng).title(formatted_address));
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentMarketLatLng, 5.0F));
        }
    }
}
