package com.example.avenuecode.androidchallenge.service;

import com.example.avenuecode.androidchallenge.model.Response;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by avenuecode on 06/12/2017.
 */

public interface GoogleService {
    @GET("geocode/json?sensor=false&key=AIzaSyDuZqK23a6E9vXAwbSWMOp-LoIw8X0YDMQ")
    Call<Response> listAddress(@Query("address") String address);
}
