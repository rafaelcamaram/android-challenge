package com.example.avenuecode.androidchallenge.utils;

import com.example.avenuecode.androidchallenge.R;

/**
 * Created by avenuecode on 06/12/2017.
 */
public interface Color {
    int YELLOW = R.color.yellowDefault;
    int GREEN = R.color.greenDefault;
    int WHITE = R.color.whiteDefault;
}