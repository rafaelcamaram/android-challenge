package com.example.avenuecode.androidchallenge.utils;

/**
 * Created by avenuecode on 18/12/2017.
 */
public interface Constants {
    String FORMATTED_COUNTRIES_ADDRESSES = "FORMATTED_COUNTRIES_ADDRESSES";
    String LAT_LNG_LOCATIONS = "LAT_LNG_LOCATIONS";
    String CURRENT_LATITUDE = "CURRENT_LATITUDE";
    String CURRENT_LONGITUDE = "CURRENT_LONGITUDE";
    String CURRENT_FORMATTED_ADDRESS = "CURRENT_FORMATTED_ADDRESS";
    String IS_BOUND = "IS_BOUND";
}
